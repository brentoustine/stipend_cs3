import {useState} from 'react'
import AppHelper from '../../../app-helper'
import { Form, Button, Row, Col, Card } from 'react-bootstrap'
import Router from 'next/router'
import View from '../../../components/View'
import Swal from 'sweetalert2'

export default () => {
    return (
        <View title="New Record">
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>New Record</h3>
                    <Card>
                        <Card.Header>Record Information</Card.Header>
                        <Card.Body>
                            <NewRecordForm/>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </View>
    )
}

const NewRecordForm = () => {

    const [categoryName,setCategoryName] = useState(undefined)
    const [type,setTypeName] = useState(undefined)
    const [amount,setAmount] = useState(0)
    const [description,setDescription] = useState("")
    const [categories,setCategories] = useState([]);

    const getCategories = (value) => {
        setTypeName(value)

        const payload = {
            method:'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            },
            body: JSON.stringify({type:value})
        }

        //send a request using fetch
        fetch(`http://localhost:9090/api/users/get-categories`, payload).then(AppHelper.toJSON).then(data => {
            setCategories(data)
        })
    }

    const createRecords = (e) => {
        e.preventDefault()

        const payload2 = {
            method:'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            },
            body: JSON.stringify({
                categoryName: categoryName,
                type: type, 
                amount: amount,
                description: description
            })
        }
        //send the request to the backend project for processing
        fetch(`http://localhost:9090/api/users/add-record`, payload2).then(AppHelper.toJSON).then(newRecordData => {
            //we will describe the procedure upon receiving the response.
            console.log(newRecordData)
            if (newRecordData === true) {
                Swal.fire('Record Added', 'The new Record was successfully created!', 'success')
                Router.push('/user/records')
            } else {
                Swal.fire('OPERATION FAILED', 'New Record was not added, please try again.', 'error')
            }
        })
    }

    return (
        <Form onSubmit={(e) => createRecords(e)}>
            <Form.Group controlId="typeName">
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" value={type} onChange={(e) => getCategories(e.target.value)}required>
                    <option value selected disabled>Select Category</option>
                    <option value="Income">Income</option>
                    <option value="expense">Expense</option>
                </Form.Control>
            </Form.Group>

            <Form.Group controlId="categoryName">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control as="select" value={categoryName} onChange={(e) => setCategoryName(e.target.value)} required>
                    <option value selected disabled>Select Category Name</option>
                    {
                        categories.map((category) => {
                            return(
                                <option key={category.id} value={category.name}>
                                    {category.name}
                                </option>
                            )
                        })
                    }
                </Form.Control>
            </Form.Group>

            <Form.Group controlId="amount">
                <Form.Label>Amount:</Form.Label>
                <Form.Control type="number" placeholder="Enter amount" value={amount} onChange={(e) => setAmount(e.target.value)} required/>
            </Form.Group>

            <Form.Group controlId="description">
                <Form.Label>Description:</Form.Label>
                <Form.Control type="text" placeholder="Enter description" value={description} onChange={(e) => setDescription(e.target.value)} required/>
            </Form.Group>

            <Button className="mt-2 w-100" variant="primary" type="submit">Submit</Button>
        </Form>
    )
}