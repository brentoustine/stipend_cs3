import { Card, Button, Row, Col, InputGroup, FormControl, Form } from 'react-bootstrap'
import {useState, useEffect} from 'react'
import AppHelper from '../../../app-helper'
import Link from 'next/link'
import View from '../../../components/View'

export default () => {
    return (
        <View title="Records">
            <h3>Records</h3>
            <InputGroup className="mb-2">
                <InputGroup.Prepend>
                    <Link href="/user/records/new">
                        <a className="btn btn-success">Add</a>
                    </Link>
                </InputGroup.Prepend>
            
            </InputGroup>
            <RecordsView />
        </View>
    )
}

const RecordsView = () => {  

const [typeName,setTypeName] = useState("") 
const [records,setRecords] = useState([]) 

useEffect(() => {
    const payload = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ AppHelper.getAccessToken()}`
          },
          body: JSON.stringify({value: typeName})
       }

       fetch(`http://localhost:9090/api/users/get-records`, payload).then(AppHelper.toJSON).then(fetchedData => {
        ;     console.log(fetchedData)
             setRecords(fetchedData.transactions.map(records => records))

            })
}, [])


    return (
        <> {records.map(record => {
            return (<Card className="mb-3 mt-3">
                            <Card.Body>
                                <Row>
                                    <Col xs={ 6 }>
                                        <h5>{record.description}</h5>
                                        <h6>
                                          <span>Category: {record.categoryName}</span> 
                                        </h6>
                                        <p>Date: {record.dateAdded}</p>
                                    </Col>
                                    <Col xs={ 6 } className="text-right">
                                        <h6>Amount: {record.amount}</h6>
                                        <h6>Balance: {record.balanceAfterTransaction}</h6>
                                        <span>{record.type}</span>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>)
            
        })}


                        
                    
     
      </>
    )
}
