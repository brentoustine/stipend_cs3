import { InputGroup, Form, Col, Container } from 'react-bootstrap'
import { Pie } from 'react-chartjs-2'
import View from '../../../components/View'
import {useState, useEffect} from 'react'
import moment from 'moment'
import AppHelper from '../../../app-helper'

export default () => {
    const [labelsArr, setLabelsArr] = useState([])
    const [dataArr, setDataArr] = useState([])
    const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))

    const data = {
        labels: labelsArr,
        datasets: [
            {
                data: dataArr,
                backgroundColor: [
                    '#003f5c',
                    '#2f4b7c',
                    '#665191',
                    '#a05195',
                    '#d45087',
                    '#f95d6a',
                    '#ff7c43',
                    '#ffa600'
                ],
                hoverBackgroundColor: [
                    '#ffa600',
                    '#ff7c43',
                    '#f95d6a',
                    '#d45087',
                    '#a05195',
                    '#665191',
                    '#2f4b7c',
                    '#003f5c'
                ]
            }
        ]
    }

   useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }, 
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })
        }

        fetch(`http://localhost:9090/api/users/get-records-breakdown-by-range`, payload).then(AppHelper.toJSON).then(records => {
            setLabelsArr(records.map(record => record.categoryName))
            setDataArr(records.map(record => record.totalAmount))
        })
    }, [fromDate, toDate])

   return(
        <View title="break it down">
            <Container className="mt-5 pt-3 pb-4 boxContainer">
                <h2 className="text-center">Category Breakdown</h2>
                <div className="d-flex">
                    <Form.Control className="w-50 m-1" type="date" value={ fromDate } onChange={ (e) => setFromDate(e.target.value) }/>
                    <Form.Control className="w-50 m-1" type="date" value={ toDate } onChange={ (e) => setToDate(e.target.value) }/>
                </div>
                <Pie data={ data } height={100}/>
            </Container>
        </View>
    )
}