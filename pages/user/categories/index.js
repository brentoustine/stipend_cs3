import { Table, Button } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import {useState, useEffect} from 'react'
import AppHelper from '../../../app-helper'
import Router from 'next/router'
import AppNavbar from '../../../components/NavBar'



export default () => {
const [typeName,setTypeName] = useState("") 
const [data,setData] = useState([])    


useEffect(() => {
    const payload = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ AppHelper.getAccessToken()}`
          },
          body: JSON.stringify({value: typeName})
       }

       fetch(`http://localhost:9090/api/users/get-categories`, payload).then(AppHelper.toJSON).then(fetchedData => 
            setData(fetchedData)
            )
}, [])

  
    return (
        <View title="Categories">
            <h3>Categories</h3>
            <Link href="/user/categories/new">
                <a className="btn btn-success mt-1 mb-3">Add</a>
            </Link>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                  {data.map(dataMapped => <tr>
                      <>
                          <td key={data.name}>{dataMapped.name}</td>
                          <td key={data.type}>{dataMapped.type}</td>
                      </>
                  </tr>)}
                </tbody>
            </Table>
        </View>
    )
}
