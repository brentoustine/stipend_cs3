import { useContext, useEffect } from 'react'; 
import UserContext from '../contexts/UserContext'
import Router from 'next/router'

export default function index() {
	const { unsetUser } = useContext(UserContext)

	//lets create a "side effect" that will allow us to clear out the local storage for us to "unmount" the current logged in user. 
	useEffect(() => {
		//describe what you want to happen.
		unsetUser(); 
		//redirect the user
        Router.push('/')
	}, [])
	return null
}