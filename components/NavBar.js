import { Navbar, Nav, Container,NavDropdown } from 'react-bootstrap';
import { useState, useContext } from 'react'
//for me to be able to create routes for each item inside the navbar i'm going to use the Link from next
import UserContext from '../contexts/UserContext'; 
import Link from 'next/link'

//i want to be able to create routes for my navbar elements

//lets create a function this time using an es6 format

export default () => {
	const { user } = useContext(UserContext)
	return(
	<Navbar bg="dark" expand="lg" fixed="top" variant="dark">

	  <Link href="/">
	    <a className="navbar-brand">STIPEND</a>
	  </Link>

	  <Navbar.Toggle aria-controls="basic-navbar-nav"/>
	  <Navbar.Collapse id="basic-navbar-nav">
	  <Nav className="mr-auto">
	  <>
	  {
	  	(user.email)
	  	?
	  	<>
	  	 <NavDropdown title="Category Section" id="navbarScrollingDropdown">

        <NavDropdown.Item href="/user/categories">My Categories</NavDropdown.Item>

        <NavDropdown.Item href="/user/categories/new">Create Category</NavDropdown.Item>

      </NavDropdown>

       <NavDropdown title="Record Section" id="navbarScrollingDropdown">

        <NavDropdown.Item href="/user/records">My Records</NavDropdown.Item>
        
        <NavDropdown.Item href="/user/records/new">Create Transaction</NavDropdown.Item>

        </NavDropdown>

       <Link href="/user/charts/category-breakdown">
	    <a className="nav-link">Stipend Chart</a>
	  </Link>

	  <Link href="/logout">
	    <a className="nav-link">Logout</a>
	  </Link>
	  	</>

	  	:
	  	<>
	  	<Link href="/register">
	    <a className="nav-link">Register</a>
	  </Link>

	  <Link href="/">
	    <a className="nav-link">Login</a>
	  </Link>

	  	</>
	  }
	  </>

	  

	 

        </Nav>
	</Navbar.Collapse>

	</Navbar>
	)
}